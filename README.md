## Installation de expressjs

```
$ npm install
```

## Lancement du serveur

```
$ npm start
```

## Test

```
$ curl localhost:8000
Hi!
```
